package com.example.g50home.audiojack

import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioTrack

class DataSenderSound {
    val mSound = DoubleArray(44100 / 10)
    val mBuffer = ShortArray(44100 / 10)
    val LOG_TAG = "DataSender"

    public enum class Action(val action: Int) {
        FORWARD(500),
        BACKWARD(300),
        LEFT(400),
        RIGHT(200),
        STOP(150)
    }

    public fun doCommand(action: Action){
        playSound(action.action);
    }

    private fun playSound(freq : Int) {

//        intentThread.client.send("bbbbbbbbbbbbbbbb")

        for (i in mSound.indices) {
            mSound[i] = Math.sin(2.0 * Math.PI * freq / 44100.0 * i.toDouble())
            mBuffer[i] = (mSound[i] * java.lang.Short.MAX_VALUE).toShort()
        }

        for (i in 1..1) {
            // AudioTrack definition
            val mBufferSize = AudioTrack.getMinBufferSize(44100,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT)

            val mAudioTrack = AudioTrack(AudioManager.STREAM_MUSIC, 44100,
                    AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
                    mBufferSize, AudioTrack.MODE_STREAM)

            // Sine wave

            mAudioTrack.setStereoVolume(AudioTrack.getMaxVolume(), AudioTrack.getMaxVolume())
            mAudioTrack.play()

            mAudioTrack.write(mBuffer, 0, mSound.size)
            mAudioTrack.stop()
            mAudioTrack.release()
        }
    }
}