package com.example.g50home.audiojack

import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioTrack

class DataSenderWebSocket {
    val LOG_TAG = "DataSender"

    public enum class Action(val action: String) {
        FORWARD("FORWARD"),
        BACKWARD("BACKWARD"),
        LEFT("LEFT"),
        RIGHT("RIGHT"),
        STOP("STOP")
    }

    public fun doCommand(action: Action){
//        playSound(action.action);
    }

}