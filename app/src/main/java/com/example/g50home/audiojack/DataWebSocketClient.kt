package com.example.g50home.audiojack

import android.util.Log
import tech.gusavila92.websocketclient.WebSocketClient
import java.net.URI

class DataWebSocketClient : WebSocketClient {
    val LOG_TAG = "DataWebSocketClient"
    var dataSender : DataSenderSound? = null;

    constructor(url:String, dsender : DataSenderSound) : super(URI(url)){
        this.dataSender = dsender;
    }

    override fun onOpen(){
        Log.d(LOG_TAG, "WebSocketClient.onOpen")
    }

    override fun onTextReceived(message: String){
        Log.d(LOG_TAG, "WebSocketClient.onTextReceived " + message)

        if(message == DataSenderWebSocket.Action.FORWARD.toString()) {
            dataSender?.doCommand(DataSenderSound.Action.FORWARD);
        }
        if(message == DataSenderWebSocket.Action.RIGHT.toString()) {
            dataSender?.doCommand(DataSenderSound.Action.RIGHT);
        }
        if(message == DataSenderWebSocket.Action.BACKWARD.toString()) {
            dataSender?.doCommand(DataSenderSound.Action.BACKWARD);
        }
        if(message == DataSenderWebSocket.Action.LEFT.toString()) {
            dataSender?.doCommand(DataSenderSound.Action.LEFT);
        }
        if(message == DataSenderWebSocket.Action.STOP.toString()) {
            dataSender?.doCommand(DataSenderSound.Action.STOP);
        }
    }

    override fun onBinaryReceived(data: ByteArray){
        Log.d(LOG_TAG, "WebSocketClient.onBinaryReceived ")
    }

    override fun onPingReceived(data: ByteArray){
        Log.d(LOG_TAG, "WebSocketClient.onPingReceived ")
    }

    override fun onPongReceived(data: ByteArray){
        Log.d(LOG_TAG, "WebSocketClient.onPongReceived")
    }

    override fun onException(e: Exception){
        Log.d(LOG_TAG, "WebSocketClient.onException " + e)
//                Log.d(LOG_TAG, "WebSocketClient.connecting...")
//                this.connect()
    }

    override fun onCloseReceived(){
        Log.d(LOG_TAG, "WebSocketClient.onCloseReceived")
        Log.d(LOG_TAG, "WebSocketClient.connecting...")
//        this.connect()
    }
}
