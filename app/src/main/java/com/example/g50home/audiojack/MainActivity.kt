package com.example.g50home.audiojack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.app.Activity




class MainActivity : AppCompatActivity() {
    val LOG_TAG = "MainActivity"
    val dataSender = DataSenderSound();
    var intentThread:Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intentThread = Intent(this, WebSocketsService::class.java);

        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.button1).setOnClickListener {
            dataSender.doCommand(DataSenderSound.Action.FORWARD);
        }
        findViewById<Button>(R.id.button2).setOnClickListener {
            dataSender.doCommand(DataSenderSound.Action.RIGHT);
        }
        findViewById<Button>(R.id.button3).setOnClickListener {
            dataSender.doCommand(DataSenderSound.Action.BACKWARD);
        }
        findViewById<Button>(R.id.button4).setOnClickListener {
            dataSender.doCommand(DataSenderSound.Action.LEFT);
        }
        findViewById<Button>(R.id.button5).setOnClickListener {
            dataSender.doCommand(DataSenderSound.Action.STOP);
        }
        Log.i(LOG_TAG, "Intent Service starting")

        // Initialize a new Intent instance
//        stopService(intentThread)
//        Log.i(LOG_TAG, "Intent Service stopped")
        startService(intentThread)
        Log.i(LOG_TAG, "Intent Service started - OK")
    }
}
