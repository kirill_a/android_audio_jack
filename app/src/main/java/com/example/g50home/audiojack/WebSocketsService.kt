package com.example.g50home.audiojack

import android.app.IntentService
import android.content.Intent
import android.os.IBinder
import android.util.Log


class WebSocketsService : IntentService("WebSocketsService") {

    val LOG_TAG = "WebSocketClient"
    public open var dataWebSocketClient: DataWebSocketClient = DataWebSocketClient("ws://prodobavki.com:3001/chat", DataSenderSound())

    override fun onCreate() {
        super.onCreate()
        Log.d(LOG_TAG, "WebSocketClient  - onCreate")
    }

    override fun onHandleIntent( workIntent:Intent) {
        Log.d(LOG_TAG, "WebSocketClient  - onHandleIntent")

        dataWebSocketClient.setConnectTimeout(10000);
        dataWebSocketClient.setReadTimeout(60000);
//        dataWebSocketCliet?.addHeader("Origin", "http://developer.example.com");
        dataWebSocketClient.enableAutomaticReconnection(5000);
        dataWebSocketClient.connect()
        dataWebSocketClient.send("aaaaaaaaaa")
        Log.d(LOG_TAG, "WebSocketClient.connected")
    }

    override fun onDestroy() {
//        dataWebSocketClient.close()
        super.onDestroy()
        Log.d(LOG_TAG, "onDestroy")
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(LOG_TAG, "onBind")
        return null
    }
}